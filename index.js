import {PianoPresenter} from "./piano/piano_presenter.js";

class Starter {
    static #presenter;

    static start() {
        if (!this.#presenter) {
            this.#presenter = new PianoPresenter();
        }
    }
}

Starter.start();
