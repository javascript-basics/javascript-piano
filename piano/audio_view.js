function getAudioElements() {
    return (document.getElementsByTagName('audio'));
}

const DEFAULT_TIME = 0;

export class AudioView {
    #audios = getAudioElements() || document.createDocumentFragment().children;

    toggleAudio(index) {
        const audio = (this.#audios.item(index) || new HTMLAudioElement());

        if (audio.paused) {
            audio.play().then();
            return;
        }

        audio.currentTime = DEFAULT_TIME;
    }
}
