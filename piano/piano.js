const NOTE_MAP = {
    0: 'C',
    1: 'Db',
    2: 'D',
    3: 'Ed',
    4: 'E',
    5: 'F',
    6: 'Gd',
    7: 'G',
    8: 'Ad',
    9: 'A',
    10: 'Bd',
    11: 'B',
}

const NOTES_IN_OCTAVE = 12;
const OCTAVE_OFFSET = 2;

export class Piano {
    #noteMap = new Map();

    constructor() {
        this.#init();
    }

    #init() {
        Object.entries(NOTE_MAP).forEach(([key, value]) => this.#noteMap.set(key, value));
    }

    getNoteWithOctave(index) {
        const noteIndex = index % NOTES_IN_OCTAVE;
        const note = this.#noteMap.get(noteIndex.toString());

        const octave = Math.floor(index / NOTES_IN_OCTAVE) + OCTAVE_OFFSET;

        return `${note}${octave}`;
    }
}
