import {PianoService} from "./piano_service.js";
import {PianoView} from "./piano_view.js";
import {AudioView} from "./audio_view.js";

export class PianoPresenter {
    #pianoService = new PianoService();
    #pianoView = new PianoView();
    #audioView = new AudioView();

    constructor() {
        this.#initListeners();
    }

    #initListeners() {
        this.#initClickListeners(this.#pianoView.whiteBtnGenerator);
        this.#initClickListeners(this.#pianoView.blackBtnGenerator);
        this.#initKeyDownListener();
    }

    #initClickListeners(btnGenerator) {
        for (let button of btnGenerator) {
            (button || new HTMLElement()).addEventListener('click', event =>
                this.#handleClickEvent(event, button));
        }
    }

    #handleClickEvent(event, button) {
        (event || new Event('click')).stopPropagation();

        const index = this.#pianoView.getButtonIndex(button);
        const noteWithOctave = this.#pianoService.getNoteWithOctave(index);

        this.#play(index, noteWithOctave, button);
    }


    #initKeyDownListener() {
        document.addEventListener('keydown', this.#handleKeyDownEvent.bind(this));
    }

    #handleKeyDownEvent({code}) {
        const index = this.#pianoService.getIndexByKeyCode(code);

        if (!index) {
            return;
        }

        const button = this.#pianoView.getButtonByIndex(index);
        const noteWithOctave = this.#pianoService.getNoteWithOctave(index);

        this.#play(index, noteWithOctave, button);
    }

    #play(index, noteWithOctave, button) {
        this.#audioView.toggleAudio(index);
        this.#pianoView.updateNoteContent(noteWithOctave);
        this.#pianoView.setResetNoteContentTimeout();
        this.#pianoView.animateBtn(button);
    }
}
