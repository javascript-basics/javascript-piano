import {Piano} from './piano.js'

const KEY_CODE_VALUE_MAP = {
    'Backquote': 0,
    'Digit1': 1,
    'Digit2': 2,
    'Digit3': 3,
    'Digit4': 4,
    'Digit5': 5,
    'Digit6': 6,
    'Digit7': 7,
    'Digit8': 8,
    'Digit9': 9,
    'Digit0': 10,
    'Minus': 11,
    'Equal': 12,
    'KeyQ': 13,
    'KeyW': 14,
    'KeyE': 15,
    'KeyR': 16,
    'KeyT': 17,
    'KeyY': 18,
    'KeyU': 19,
    'KeyI': 20,
    'KeyO': 21,
    'KeyP': 22,
    'BracketLeft': 23,
    'BracketRight': 24,
    'KeyA': 25,
    'KeyS': 26,
    'KeyD': 27,
    'KeyF': 28,
    'KeyG': 29,
    'KeyH': 30,
    'KeyJ': 31,
    'KeyK': 32,
    'KeyL': 33,
    'Semicolon': 33,
    'Quote': 34,
    'KeyZ': 35,
    'KeyX': 36,
    'KeyC': 37,
    'KeyV': 38,
    'KeyB': 39,
    'KeyN': 40,
    'KeyM': 41,
    'Key': 42,
    'Comma': 43,
    'Period': 44,
    'Slash': 45,
    'ArrowLeft': 46,
    'ArrowRight': 47,
}

export class PianoService {
    #piano = new Piano();
    #keyCodeValueMap = new Map();

    constructor() {
        this.#init();
    }

    #init() {
        Object.entries(KEY_CODE_VALUE_MAP).forEach(([key, value]) => this.#keyCodeValueMap.set(key, value));
    }

    getIndexByKeyCode(keyCodeValue) {
        return this.#keyCodeValueMap.get(keyCodeValue);
    }

    getNoteWithOctave(index) {
        return this.#piano.getNoteWithOctave(index);
    }
}
