function getElementByClassName(className) {
    return (document.getElementsByClassName(className) || document.createDocumentFragment().children).item(0);
}

function* elementGenerator(className) {
    const buttons = document.getElementsByClassName(className);
    for (let i = 0; i < buttons.length; i++) {
        yield buttons.item(i);
    }
}

const KEY_CLASS = 'key-value';
const KEY_HOVER_CLASS = 'key-value-hover';

const NOTE_CLASS = 'note-value';
const DEFAULT_NOTE_VALUE = 'Piano';
const HOVER_PROPAGATION_CLASS = 'hover-propagation';

const BTN_CLASS = 'btn';
const WHITE_BTN_CLASS = 'white-btn';
const BLACK_BTN_CLASS = 'black-btn';
const OPACITY_ENDING_STRING = 'opacity';

const ANIMATION_TIMEOUT = 1500;
const RESET_TIMEOUT = 2500;

export class PianoView {
    #noteElement = Object.create(HTMLElement.prototype, {});
    #noteTimeout;

    get whiteBtnGenerator() {
        return elementGenerator(WHITE_BTN_CLASS);
    }

    get blackBtnGenerator() {
        return elementGenerator(BLACK_BTN_CLASS);
    }

    constructor() {
        this.#init();
    }

    #init() {
        this.#noteElement = getElementByClassName(NOTE_CLASS);
        this.#addBlackButtonsMouseEvents();
    }

    #addBlackButtonsMouseEvents() {
        const buttons = this.blackBtnGenerator;

        const eventHandler = button => {
            const whiteButton = (button || new HTMLElement()).closest(`.${WHITE_BTN_CLASS}`) || new HTMLElement();
            whiteButton.classList.toggle(HOVER_PROPAGATION_CLASS)
        }

        for (let button of buttons) {
            (button || new HTMLElement()).addEventListener('mouseover', eventHandler.bind(this, button));
            (button || new HTMLElement()).addEventListener('mouseleave', eventHandler.bind(this, button));
        }
    }

    updateNoteContent(content) {
        this.#noteElement.innerText = content;
    }

    setResetNoteContentTimeout() {
        if (this.#noteTimeout) {
            clearTimeout(this.#noteTimeout);
        }

        this.#noteTimeout = setTimeout(this.updateNoteContent.bind(this, DEFAULT_NOTE_VALUE), RESET_TIMEOUT);
    }

    getButtonIndex(button) {
        return [...document.getElementsByClassName(BTN_CLASS)].indexOf(button)
    }

    getButtonByIndex(i) {
        return (document.getElementsByClassName(BTN_CLASS) || document.createDocumentFragment().children)
            .item(i);
    }

    animateBtn(button) {
        const buttonColorClass = [...(button || new HTMLElement()).classList]
            .find(className => [WHITE_BTN_CLASS, BLACK_BTN_CLASS].includes(className));

        const buttonOpacityClass = `${buttonColorClass}-${OPACITY_ENDING_STRING}`;
        this.#setOpacityTimeout(button, buttonOpacityClass);

        const key = PianoView.#getKeyFromButton(button);
        this.#setOpacityTimeout(key, KEY_HOVER_CLASS);
    }

    #setOpacityTimeout(element, className) {
        (element || new HTMLElement()).classList.add(className);

        setTimeout(() => (element || new HTMLElement()).classList.remove(className), ANIMATION_TIMEOUT);
    }

    static #getKeyFromButton(button) {
        return ((button || new HTMLElement()).getElementsByClassName(KEY_CLASS)
            || document.createDocumentFragment().children).item(0);
    }
}
